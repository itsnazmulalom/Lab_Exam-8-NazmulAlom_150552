<?php
namespace App\ProfilePicture;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class ProfilePicture extends DB{
    public $id="";
    public $person_name="";
    public $profile_picture="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('person_name',$postVariableData)){
            $this->person_name = $postVariableData['person_name'];
        }

        if(array_key_exists('img_name',$postVariableData)){
            $this->profile_picture = $postVariableData['img_name'];
        }
    }



//    public function store(){
//
//    $arrData = array( $this->person_name, $this->profile_picture);
//
//    $sql = "Insert INTO profile_picture(person_name, picture_address) VALUES (?,?)";
//    $STH = $this->DBH->prepare($sql);
//
//    $result = $STH->execute($arrData);
//
//     if($result)
//            Message::message("Success! Data Has Been Inserted Successfully :)");
//     else
//            Message::message("Failed! Data Has Not Been Inserted Successfully :(");
//
//
//        Utility::redirect('create.php');
//
//
//    }// end of store method
    public function store()
    {
//        $path='C:\xampp\htdocs\Lab_Exam8_Mohammad_Younus_SEIP149918_B36_Atomic_Project\resource\img';
//        $uploadedFile = $path.basename($this->profile_picture);
//        move_uploaded_file($_FILES['picture']['tmp_name'], $uploadedFile);


        $arrData = array($this->person_name, $this->profile_picture);

        $sql = "insert into profile_picture(person_name, profile_picture) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql); //create a object
        $result = $STH->execute($arrData);
        if ($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }


 public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from profile_picture WHERE is_deleted="0"');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH=$this->DBH->query('SELECT * from profile_picture WHERE id='.$this->id);
        $fetchMode=strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ')>0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);
        $arrAllData=$STH->fetch();
        return $arrAllData;
    }//end of view()


public function update(){
    $arrData = array( $this->person_name, $this->profile_picture);
    $sql="UPDATE profile_picture SET person_name=? , picture_address=? where id=".$this->id;
    $STH = $this->DBH->prepare($sql);
    $STH->execute($arrData);
    Utility::redirect('index.php');

}//end of update


    public function delete(){

        $sql = "Delete from profile_picture where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result=$STH->execute();
        if($result)
            Message::message("Success! Data Has Been deleted ");
        else
            Message::message("Failed! Data Has Not Been deleted ");


        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "Update profile_picture SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result=$STH->execute();
        if($result)
            Message::message("Success! Data Has Been trashed");
        else
            Message::message("Failed! Data Has Not Been trashed");


        Utility::redirect('index.php');


    }// end of trash()

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from profile_picture where is_deleted <>'0' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();

    public function recover(){

        $sql = "Update profile_picture SET is_deleted='0' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }














}// end of BookTitle Class

?>








